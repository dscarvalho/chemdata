__author__ = 'danilo.silva@cdts.fiocruz.br'

import sys
import re
import csv
import copy
from chemdata.worker.smiles import set_jobs
from chemdata.export import SmilesExport

from chemdata.data_access.pubchem_dump import PubChemMappingReader
from chemdata.data_access.chemdata_pg import ChemDataStore, Compound, CompoundPatent, CompoundSynonym, CompoundAttributesReference, CompoundAttributes
from pprint import pprint

PREFIX_SWISSADME = 'SwissAdme'
PREFIX_ADMETSAR = 'AdmetSar'

def mapSwissAdme():
    return  {
        "Physicochemical_Properties_Molecular_weight": "molecular_weight",
        #"???1": "molecular_weight_unit",
        "Physicochemical_Properties_Num._heavy_atoms": "num_heavy_atoms",
        "Physicochemical_Properties_Num._arom._heavy_atoms": "num_arom_heavy_atoms",
        "Physicochemical_Properties_Fraction_Csp3": "fraction_csp3",
        "Physicochemical_Properties_Num._rotatable_bonds": "num_rotatable_bonds",
        "Physicochemical_Properties_Num._H-bond_acceptors": "num_h_bond_acceptors",
        "Physicochemical_Properties_Num._H-bond_donors": "num_h_bond_donors",
        "Physicochemical_Properties_Molar_Refractivity": "molar_refractivity",
        "Physicochemical_Properties_TPSA": "tpsa",
        #"???2": "tpsa_unit",

        "Lipophilicity_Log_Po/w_(iLOGP)": "ilogp",
        "Lipophilicity_Log_Po/w_(XLOGP3)": "xlogp3",
        "Lipophilicity_Log_Po/w_(WLOGP)": "wlogp",
        "Lipophilicity_Log_Po/w_(MLOGP)": "mlogp",
        "Lipophilicity_Log_Po/w_(SILICOS-IT)": "silicos_it",
        "Lipophilicity_Consensus_Log_Po/w": "consensus_log_p_ow",

        "Water_Solubility_Log_S_(ESOL)": "log_s_esol",
        "Water_Solubility_ESOL_Solubility_(mg/ml)": "solubility_esol_mg_ml",
        "Water_Solubility_ESOL_Solubility_(mol/l)": "solubility_esol_mol_l",
        "Water_Solubility_ESOL_Class": "class_esol",
        "Water_Solubility_Log_S_(Ali)": "log_s_ali",
        "Water_Solubility_Ali_Solubility_(mg/ml)": "solubility_ali_mg_ml",
        "Water_Solubility_Ali_Solubility_(mol/l)": "solubility_ali_mol_l",
        "Water_Solubility_Ali_Class": "class_ali",
        "Water_Solubility_Log_S_(SILICOS-IT)": "log_s_silicos_it",
        "Water_Solubility_Silicos-IT_Solubility(mg/ml)": "solubility_silicos_it_mg_ml",
        "Water_Solubility_Silicos-IT_Solubility_(mol/l)": "solubility_silicos_it_mol_l",
        "Water_Solubility_Silicos-IT_Class": "class_silicos_it",

        "Pharmacokinetics_GI_absorption": "gi_absorption",
        "Pharmacokinetics_BBB_permeant": "bbb_permeant",
        "Pharmacokinetics_P-gp_substrate": "p_gp_substrate",
        #"???3": "p_gp_inhibitor",
        "Pharmacokinetics_CYP1A2_inhibitor": "cyp1a2_inhibitor",
        "Pharmacokinetics_CYP2C19_inhibitor": "cyp2c19_inhibitor",
        "Pharmacokinetics_CYP2C9_inhibitor": "cyp2c9_inhibitor",
        "Pharmacokinetics_CYP2D6_inhibitor": "cyp2d6_inhibitor",
        "Pharmacokinetics_CYP3A4_inhibitor": "cyp3a4_inhibitor",
        #"???4": "cyp2c9_substrate",
        #"???5": "cyp2d6_substrate",
        #"???6": "cyp3a4_substrate",
        "Pharmacokinetics_Log_Kp_(skin_permeation)": "skin_permeation",

        "Druglikeness_Lipinski": "lipinski",
        "Druglikeness_Ghose": "ghose",
        "Druglikeness_Veber": "veber",
        "Druglikeness_Egan": "egan",
        "Druglikeness_Muegge": "muegge",
        "Druglikeness_Bioavailability_Score": "bioavailability_score",

        "Medicinal_Chemistry_PAINS": "pains",
        "Medicinal_Chemistry_Brenk": "brenk",
        "Medicinal_Chemistry_Leadlikeness": "leadlikeness",
        "Medicinal_Chemistry_Synthetic_accessibility": "synthetic_accessibility",
    }


def constructCompoundAttributes(
    compound: Compound, 
    compound_attributes_reference: CompoundAttributesReference, 
    attr_map: dict, 
    csv_line
    ) -> CompoundAttributes:
    
    data = dict()

    booleanAttributes = [
        'Druglikeness_Ghose',
        'Druglikeness_Egan',
        'Druglikeness_Veber',
        'Druglikeness_Muegge'
    ]

    for key, attr in attr_map.items():

        csv_key = PREFIX_SWISSADME + '_' + key

        if csv_line[csv_key] == '':
            csv_line[csv_key] = None

        if csv_line[csv_key] == 'No':
            csv_line[csv_key] = False
        
        if csv_line[csv_key] == 'Yes':
            csv_line[csv_key] = True

        if key in booleanAttributes:
            if csv_line[csv_key] == '0':
                csv_line[csv_key] = False
            else:
                csv_line[csv_key] = True

        data[attr] = csv_line[csv_key]

    data['compound_id'] = compound.id
    data['reference'] = compound_attributes_reference.id

    return data


def persistSmilesByCSV(
    data_store, 
    csv_file, 
    attr_map: dict, 
    compound_attributes_reference: CompoundAttributesReference
    ):

    with open(csv_file) as file:
        reader = csv.DictReader(file, delimiter='\t')
        count = 0
        processed_smiles = set()

        for line in reader:

            if line['smiles'] in processed_smiles:
                continue

            compound = data_store.get_compound_by_smiles(line['smiles'])

            data = constructCompoundAttributes(
                compound,
                compound_attributes_reference,
                attr_map,
                line
            )
            
            count += 1
            print(count, line['smiles'], '\n\n')

            compound_attributes = CompoundAttributes(**data)

            data_store.insert_compound_attributes(compound_attributes)

            processed_smiles.add(line['smiles'])
           
    data_store.commit()


def main(argv):
    data_store = ChemDataStore("postgresql://postgres:ace_CDTS_2019@157.86.38.58/chemdata")
    attr_map_swissadme = mapSwissAdme()
    compound_attributes_reference = data_store.get_compound_attributes_reference_by_name(PREFIX_SWISSADME)
    
    persistSmilesByCSV(
        data_store,
        argv[1],
        attr_map_swissadme,
        compound_attributes_reference
    )  
    
    data_store.commit()

if __name__ == "__main__":
    main(sys.argv)
