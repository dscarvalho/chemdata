#!/usr/bin/env python3
__author__ = 'danilo.silva@cdts.fiocruz.br'

import sys
import os
import random
import json
import pickle
import psycopg2
from typing import List, Set, Dict
from psycopg2 import sql
from psycopg2.extras import DictCursor
from chemdata.analysis.groupfinder import GroupFinder

ATTR_CHAIN = ["molweight", "logp"]
BASE_ATTRS = {"molweight", "logp"}
CACHE_DIR = "/home/ubuntu/chemdata/data/distr_cache"


def sample_interval(field: str, min_val: float, max_val: float, size: int, cursor: DictCursor):
    query = sql.SQL("select smiles from zinc where {field} >= %(min)s and {field} <= %(max)s;").format(field=sql.Identifier(field))
    cursor.execute(query, {"min": min_val, "max": max_val})
    smiles = [row["smiles"] for row in cursor.fetchall()]
    print("SMILES selected:", len(smiles))

    return random.sample(smiles, size)


def query_interval(field: str, min_val: float, max_val: float, cursor: DictCursor):
    query = sql.SQL("select smiles from zinc where {field} >= %(min)s and {field} <= %(max)s;").format(field=sql.Identifier(field))
    cursor.execute(query, {"min": min_val, "max": max_val})
    smiles = set([row["smiles"] for row in cursor.fetchall()])
    print("SMILES selected %s [%f, %f]:" % (field, min_val, max_val), len(smiles))

    return smiles

def org_func_interval(group: str, min_val: int, max_val: int, smiles_sample: Set[str], groupfinder: GroupFinder):
    org_groups = dict([(smiles, groupfinder.count_groups(smiles)) for smiles in smiles_sample])
    smiles = set([smiles for smiles in org_groups if (min_val <= org_groups[smiles][group] <= max_val)])

    return smiles


def sample(attr_distrs: dict, cursor: DictCursor, groupfinder: GroupFinder, attr_queue: List[str] = ATTR_CHAIN,
           sample_smiles: Set[str] = set(), chain: List[str] = [], cache: Set[str] = set(), results: dict = dict()):
    next_chain = []
    if (len(attr_queue) > 0):
        attr = attr_queue[0]

        for attr_range in attr_distrs[attr]:
            smiles = set()
            chain_link = attr + "_" + str(attr_range["start"]) + ":" + str(attr_range["end"])
            if (chain_link in cache):
                print("Cached:", chain_link)
                smiles = pickle.load(open(os.path.join(CACHE_DIR, chain_link + ".pickle"), "rb"))
            else:
                if (attr in BASE_ATTRS):
                    smiles = query_interval(attr, attr_range["start"], attr_range["end"], cursor)
                else:
                    smiles = org_func_interval(attr, attr_range["start"], attr_range["end"], smiles_sample=sample_smiles,
                                               groupfinder=groupfinder)

                pickle.dump(smiles, open(os.path.join(CACHE_DIR, chain_link + ".pickle"), "wb"), pickle.HIGHEST_PROTOCOL)
                cache.add(chain_link)

            if (len(sample_smiles) > 0):
                smiles = sample_smiles.intersection(smiles)

            next_chain = chain + [chain_link]
            print("->".join(next_chain) + " :: ", len(smiles))

            if (len(smiles) > 0):
                sample(attr_distrs, cursor, groupfinder, attr_queue=attr_queue[1:], sample_smiles=smiles,
                       chain=next_chain, cache=cache, results=results)
    else:
        results["->".join(next_chain)] = sample_smiles
        print("->".join(next_chain) + " :: ", len(sample_smiles))





def main(argv):
    connstring = argv[1]
    conn = psycopg2.connect(connstring)
    cursor = conn.cursor(cursor_factory=DictCursor)

    attr_distr_dir = argv[2]
    attr_file_paths = os.listdir(attr_distr_dir)
    attr_distrs = dict()
    for file_path in attr_file_paths:
        print(os.path.join(attr_distr_dir, file_path))
        with open(os.path.join(attr_distr_dir, file_path)) as attr_distr_file:
            distr_data = json.load(attr_distr_file)
            attr_distrs[distr_data["attr"]] = [attr_rng for attr_rng in distr_data["ranges"] if attr_rng["freq"] > 0]

    groupfinder = GroupFinder(argv[3])
    cache_keys = set([filepath.replace(".pickle", "") for filepath in os.listdir(CACHE_DIR)])
    print("Cache keys:", cache_keys)

    critical_set = dict()
    sample(attr_distrs, cursor, groupfinder, cache=cache_keys, results=critical_set)

    with open("results.json", "w") as results_file:
        json.dump(critical_set, results_file, indent=2)




if __name__ == "__main__":
    main(sys.argv)