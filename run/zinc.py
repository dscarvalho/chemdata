__author__ = 'danilo.silva@cdts.fiocruz.br'

import sys
import os
from chemdata.worker.zinc import set_jobs


def main(argv):
    tranche_files = [os.path.join(argv[1], filename) for filename in os.listdir(argv[1]) if (filename.endswith(".gz"))]

    set_jobs(tranche_files, argv[2])


if __name__ == "__main__":
    main(sys.argv)
