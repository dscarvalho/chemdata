__author__ = 'danilo.silva@cdts.fiocruz.br'

import sys
import re
from chemdata.worker.smiles import set_jobs
from chemdata.export import SmilesExport
from chemdata.data_access.chemdata_pg import ChemDataStore, Compound, CompoundPatent, CompoundSynonym
from pprint import pprint

def main(argv):

    if (argv[1] == "export"):
        SmilesExport(argv[2], argv[3], argv[4])

    else:
        completed_smiles = set()
        completed_rgx = re.compile(r"^#(\d+);$")

        if (len(argv) > 3):
            with open(argv[3]) as completed_file:
                for line in completed_file:
                    cid_match = completed_rgx.match(line)
                    if (cid_match):
                        completed_smiles.add(int(cid_match.group(1)))

        set_jobs(argv[1], argv[2], completed_smiles)


if __name__ == "__main__":
    main(sys.argv)
