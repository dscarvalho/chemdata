__author__ = 'danilo.silva@cdts.fiocruz.br'

import sys
import re
from chemdata.worker.pubchem import set_jobs


def main(argv):
    completed_cids = set()
    completed_rgx = re.compile(r"^#(\d+);$")

    if (len(argv) > 2):
        with open(argv[2]) as completed_file:
            for line in completed_file:
                cid_match = completed_rgx.match(line)
                if (cid_match):
                    completed_cids.add(int(cid_match.group(1)))

    set_jobs(argv[1], completed_cids)


if __name__ == "__main__":
    main(sys.argv)
