__author__ = 'danilo.silva@cdts.fiocruz.br'

import sys
import os
from typing import Dict

from chemdata.data_access.pubchem_dump import PubChemMappingReader, PubChemPropertiesReader
from chemdata.data_access.chemdata_pg import ChemDataStore, Compound, CompoundPatent, CompoundSynonym, CompoundAttributes, CompoundAttributesReference

SMILES_FILENAME = "CID-SMILES"
IUPAC_FILENAME = "CID-IUPAC"
INCHI_FILENAME = "CID-InChI-Key"
PATENTS_FILENAME = "CID-Patent"
SYNONYMS_FILENAME = "CID-Synonym-filtered"


def get_compound(data_store, cid_id_map, cid, title):
    compound = Compound(cid=cid, title=title)
    data_store.insert_compound(compound)
    compound.id = data_store.cur_max_id + 1
    data_store.cur_max_id += 1
    cid_id_map[cid] = compound.id

    return compound


def main(argv):
    print("Creating database...")
    data_store = ChemDataStore("postgresql://postgres:ace_CDTS_2019@localhost/chemdata")
    cid_id_map: Dict[int, int] = dict()

    # i = 0
    # print("Transfering synonyms...")
    # syn_buffer = list()
    # synonym_reader = PubChemMappingReader(os.path.join(argv[1], SYNONYMS_FILENAME))
    # for (cid, synonym) in synonym_reader:
    #     compound_id = 0
    #     if (cid not in cid_id_map):
    #         compound = get_compound(data_store, cid_id_map, cid, synonym[0])
    #         compound_id = compound.id
    #     else:
    #         compound_id = cid_id_map[cid]
    #
    #     compound_synonym = CompoundSynonym()
    #     compound_synonym.compound_id = compound_id
    #     compound_synonym.synonym = synonym[0]
    #
    #     syn_buffer.append(compound_synonym)
    #
    #     i += 1
    #     if (i % 6000000 == 0):
    #         print(i)
    #         data_store.commit()
    #         for compsyn in syn_buffer:
    #             data_store.insert_compound_synonym(compsyn)
    #         data_store.commit()
    #         syn_buffer = list()
    #         print("Commited.")
    #
    # data_store.commit()
    #
    # compound_attrs = {
    #     "SMILES": PubChemMappingReader(os.path.join(argv[1], SMILES_FILENAME)),
    #     "IUPAC": PubChemMappingReader(os.path.join(argv[1], IUPAC_FILENAME)),
    #     "INCHI": PubChemMappingReader(os.path.join(argv[1], INCHI_FILENAME))
    # }
    #
    # for attr in compound_attrs:
    #     i = 0
    #     print("Transfering %s..." % attr)
    #     attrib_buffer = list()
    #     cid_attrib_map = dict()
    #     attr_reader = compound_attrs[attr]
    #     for (cid, attrib) in attr_reader:
    #         if (attr in ["SMILES", "IUPAC"]):
    #             cid_attrib_map[cid] = attrib[0]
    #         elif (attr == "INCHI"):
    #             cid_attrib_map[cid] = attrib
    #
    #         i += 1
    #         if (i % 1000000 == 0):
    #             print(i)
    #             compounds = data_store.get_compound_ids_by_cid(list(cid_attrib_map.keys()))
    #             for compound in compounds:
    #                 if (attr in ["SMILES", "IUPAC"]):
    #                     attrib_buffer.append({"id": compound.id, attr.lower(): cid_attrib_map[compound.cid]})
    #                 elif (attr == "INCHI"):
    #                     attrib_buffer.append({"id": compound.id,
    #                                           attr.lower(): cid_attrib_map[compound.cid][0],
    #                                           "inchi_key": cid_attrib_map[compound.cid][1]})
    #
    #             data_store.update_compound_mappings(attrib_buffer)
    #             attrib_buffer = list()
    #             cid_attrib_map = dict()
    #             print("Commited.")
    #
    #     print(i)
    #     compounds = data_store.get_compound_ids_by_cid(list(cid_attrib_map.keys()))
    #     for compound in compounds:
    #         if (attr in ["SMILES", "IUPAC"]):
    #             attrib_buffer.append({"id": compound.id, attr.lower(): cid_attrib_map[compound.cid]})
    #         elif (attr == "INCHI"):
    #             attrib_buffer.append({"id": compound.id,
    #                                   attr.lower(): cid_attrib_map[compound.cid][0],
    #                                   "inchi_key": cid_attrib_map[compound.cid][1]})
    #
    #     data_store.update_compound_mappings(attrib_buffer)
    #     del attrib_buffer
    #     del cid_attrib_map
    #     print("Commited.")
    #
    # i = 0
    # print("Transfering patents...")
    # cid_patent_map = dict()
    # patents_reader = PubChemMappingReader(os.path.join(argv[1], PATENTS_FILENAME))
    # for (cid, patent) in patents_reader:
    #     if (cid not in cid_patent_map):
    #         cid_patent_map[cid] = list()
    #
    #     cid_patent_map[cid].append(patent[0])
    #
    #     i += 1
    #     if (i % 1000000 == 0):
    #         print(i)
    #         compounds = data_store.get_compound_ids_by_cid(list(cid_patent_map.keys()))
    #         for compound in compounds:
    #             for pat in cid_patent_map[compound.cid]:
    #                 compound_patent = CompoundPatent()
    #                 compound_patent.compound_id = compound.id
    #                 compound_patent.patent = pat
    #
    #                 data_store.insert_compound_patent(compound_patent)
    #
    #         data_store.commit()
    #         cid_patent_map = dict()
    #         print("Commited.")
    #
    # print(i)
    # compounds = data_store.get_compound_ids_by_cid(list(cid_patent_map.keys()))
    # for compound in compounds:
    #     for pat in cid_patent_map[compound.cid]:
    #         compound_patent = CompoundPatent()
    #         compound_patent.compound_id = compound.id
    #         compound_patent.patent = pat
    #
    #         data_store.insert_compound_patent(compound_patent)
    #
    # data_store.commit()
    # print("Commited.")

    print("Transfering attributes...")
    compound_attrs_ref = CompoundAttributesReference(name="PubChem")
    compound_attrs_ref.id = 1
    # data_store.insert_compound_attributes_reference(compound_attrs_ref)
    compound_attrs_ref = data_store.get_compound_attributes_reference_by_name("PubChem")

    properties_reader = PubChemPropertiesReader(argv[1])
    for compound_props in properties_reader:
        compound = data_store.get_compound_by_cid(compound_props["cid"])
        if (compound is not None):
            # print(compound.title, "...")

            attrs = CompoundAttributes()
            attrs.compound_id = compound.id
            attrs.reference = compound_attrs_ref.id

            compound.formula = compound_props["formula"]
            props = dict(compound_props)
            del props["formula"]

            for attr in props:
                setattr(attrs, attr, props[attr])

            attrs.molecular_weight_unit = "g/mol"
            attrs.tpsa_unit = "A^2"

            data_store.insert_compound_attributes(attrs)
            data_store.commit()





if __name__ == "__main__":
    main(sys.argv)
