from distutils.core import setup

setup(
    name='chemdata',
    version='0.85',
    packages=['run', 'chemdata', 'chemdata.parser', 'chemdata.worker', 'chemdata.fetcher', 'chemdata.data_access'],
    url='',
    license='',
    author='Danilo S. Carvalho',
    author_email='danilo.silva@cdts.fiocruz.br',
    description='',
    install_requires=[
        'joblib',
        'requests',
        'numpy'
    ],
    extras_require={
        'pg_json': ['psycopg2']
    }
)
