__author__ = 'danilo.silva@cdts.fiocruz.br'

import re
import io
import requests
import urllib.parse

from .base import WebFetcher, DataFormat
from chemdata.parser import SmilesAdmetSarParser


BASE_URL = "http://59.78.98.101/admetsar1/predict/?action=A&smiles="

class SmilesAdmetSarFetcher(WebFetcher):
    def __init__(self, parser: SmilesAdmetSarParser):
        super().__init__(parser, DataFormat.HTML, BASE_URL)

    def fetch(self, smiles: str):
        data = urllib.parse.quote(smiles)
        smiles_html = BASE_URL + data
        req = requests.get(smiles_html)

        return self.parser.parse(req.text)
