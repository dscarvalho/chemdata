__author__ = 'danilo.silva@cdts.fiocruz.br'

import re
import time
import random
import requests
from .base import WebFetcher, DataFormat
from chemdata.parser import SmilesSwissAdmeParser

BASE_URL = "http://swissadme.ch/index.php"

BASE_URL_RGX = re.compile(r"results/\d+/swissadme\.csv")

class SmilesSwissAdmeFetcher(WebFetcher):
    def __init__(self, parser: SmilesSwissAdmeParser):
        super().__init__(parser, DataFormat.HTML, BASE_URL)

    def fetch(self, smiles: str):
        data = {'smiles': smiles}
       
        smiles_html = super().fetch(data, "POST")

        match = BASE_URL_RGX.search(smiles_html)
        csv_url = "http://swissadme.ch/" + match.group(0)

        req = None
        success = False
        while (not success):
            try:
                req = requests.get(csv_url)
                success = True
            except Exception as e:
                print("F", csv_url)
                success = False
                time.sleep(random.random() * 5)

        return self.parser.parse(req.text)
