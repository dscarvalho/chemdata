__author__ = 'danilo.silva@cdts.fiocruz.br'

from .base import WebFetcher, DataFormat
from chemdata.parser import PUGViewSynsetParser

BASE_URL = "https://pubchem.ncbi.nlm.nih.gov/rest/pug_view/index/compound/%d/JSON"


class PUGViewFetcher(WebFetcher):
    def __init__(self, parser: PUGViewSynsetParser):
        super().__init__(parser, DataFormat.JSON, BASE_URL)

    def fetch(self, cid: int):
        self.base_url = BASE_URL % cid
        return super().fetch(dict())
