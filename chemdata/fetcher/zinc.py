__author__ = 'danilo.silva@cdts.fiocruz.br'

from .base import WebFetcher, DataFormat

BASE_URL = "http://files.docking.org/"
BASE_HEADERS = {
    "Host": "zinc.docking.org",
    "Connection": "keep-alive",
    "Upgrade-Insecure-Requests": "1",
    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.136 Safari/537.36",
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
    "Referer": "http://zinc.docking.org/substances/subsets/BA/?page=",
    "Accept-Encoding": "gzip, deflate",
    "Accept-Language": "en-US,en;q=0.9"
}


class ZincFetcher(WebFetcher):
    def __init__(self):
        super().__init__(None, DataFormat.CSV, BASE_URL)

    def fetch(self, set_url: str):
        if (set_url.startswith("http")):
            self.base_url = set_url
        else:
            self.base_url = BASE_URL + set_url

        return super().fetch(dict())
