__author__ = 'danilo.silva@cdts.fiocruz.br'

import time
import requests
from requests.exceptions import ConnectionError
from enum import Enum
from typing import Dict
from chemdata.parser.base import ChemDataParser


class DataFormat(Enum):
    HTML = 0
    JSON = 1
    CSV = 2


class WebFetcher:
    def __init__(self, parser: ChemDataParser, data_format: DataFormat, base_url: str):
        self.parser: ChemDataParser = parser
        self.base_url: str = base_url
        self.data_format: DataFormat = data_format
        super().__init__()

    def fetch(self, params: Dict[str, str], method: str = "GET", headers: Dict[str, str] = None):
        # print("Fetching", self.base_url, "...")
        success: bool = False
        while (not success):
            try:
                req = None
                if (method == "GET"):
                    req = requests.get(self.base_url, params, timeout=7200)
                elif (method == "POST"):
                    req = requests.post(self.base_url, data=params, timeout=7200)

                if (req.status_code == 404):
                    # print("F", self.base_url.split("/")[-2], req.status_code)
                    print("F", self.base_url, req.status_code, req.url)
                    success = False
                    break
                elif (req.status_code not in [200, 301]):
                    # print("F", self.base_url.split("/")[-2], req.status_code)
                    print("F", self.base_url, req.status_code, req.url)
                    success = False
                    time.sleep(5)
                else:
                    success = True
            except ConnectionError as e:
                print(e, e.args)
                success = False
                time.sleep(5)
            except Exception as e:
                print(e, e.args)
                success = False
                time.sleep(5)

        data = None
        if (success):
            if (self.data_format == DataFormat.HTML):
                data = req.text
            elif (self.data_format == DataFormat.JSON):
                data = req.json()
            elif (self.data_format == DataFormat.CSV):
                data = req.text
            else:
                data = req.text
        else:
            return None

        if (self.parser is None):
            return data
        else:
            return self.parser.parse(data)

