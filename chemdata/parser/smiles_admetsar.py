__author__ = 'danilo.silva@cdts.fiocruz.br'

import uuid
import sys
import io

from typing import List, Dict
from .base import ChemDataParser
from csv import DictReader
from bs4 import BeautifulSoup


class SmilesAdmetSarParser(ChemDataParser):
    def parse(self, data):
        count = 0
        soup = BeautifulSoup(data, 'html5lib')
        tables = soup.select(".content .compound_profile tr")

        classification = []
        regression = []

        for line in tables:
            children = line.findChildren("td", recursive=False)
            
            if len(children) < 3:
                continue 

            first_value, second_value, third_value = \
                children[0].get_text(), \
                children[1].get_text(), \
                children[2].get_text()

            if count==0:
                first_label, second_label, third_label = \
                    first_value, second_value, third_value
                count += 1
                continue
            
            if second_value == "Value" and third_value == "Unit":
                first_label, second_label, third_label = \
                    first_value, second_value, third_value
                count += 1
                continue
            
            if second_label == "Result" and third_label == "Probability":
                classification.append({first_label: first_value,
                                       second_label: second_value,
                                       third_label: third_value})

            elif second_label == "Value" and third_label == "Unit":
                regression.append({first_label: first_value,
                                   second_label: second_value,
                                   third_label: third_value})

            count += 1

        smiles = soup.find("input", {"id": "id_smiles"}).get("value")

        report = {
            "smiles": smiles,
            "classification": classification,
            "regression": regression
        }

        return report


