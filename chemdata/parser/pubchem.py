__author__ = 'danilo.silva@cdts.fiocruz.br'

from typing import List, Dict
from copy import deepcopy
from .base import ChemDataParser

TDV_WIKT_SCHEMA = {
    "wikid": 0,
    "title": "",
    "langs": {
        "English": {
            "pos_order": [
                "noun"
            ],
            "meanings": {
                "noun": []
            },
            "synonyms": {
                "noun": []
            }
        }
    }
}

TDV_MEANING_SCHEMA = {
    "meaning": ""
}


class PUGViewSynsetParser(ChemDataParser):
    def parse(self, data):
        chemsyns: Dict = dict()
        cid: int = int(data["Record"]["RecordNumber"])
        title: str = data["Record"]["RecordTitle"]
        info: List[Dict] = data["Record"]["Information"]
        synlist: List[str] = [synname["String"] for synname in info[0]["Value"]["StringWithMarkup"]]

        chemsyns["cid"] = cid
        chemsyns["name"] = title
        chemsyns["synonyms"] = synlist
        chemsyns["definitions"] = list()

        for definition in [attr["Value"]["StringWithMarkup"][0]["String"] for attr in info if "StringWithMarkup" in attr["Value"]]:
            if (len(definition) > 120):
                chemsyns["definitions"].append(definition.replace("\n", " ").strip())

        return chemsyns


class PUGViewTDVSynsetParser(ChemDataParser):
    def parse(self, data):
        chemsyns: List[Dict] = []
        title: str = data["Record"]["RecordTitle"]
        info: List[Dict] = data["Record"]["Information"]
        synlist: List[str] = [synname["String"] for synname in info[0]["Value"]["StringWithMarkup"]]
        chem_concepts: Dict = deepcopy(TDV_WIKT_SCHEMA)
        chem_concepts["title"] = title

        if (title not in synlist):
            synlist.append(title)

        for definition in [attr["Value"]["StringWithMarkup"][0]["String"] for attr in info if "StringWithMarkup" in attr["Value"]]:
            if (len(definition) > 120):
                concept: Dict = deepcopy(TDV_MEANING_SCHEMA)
                concept["meaning"] = definition
                chem_concepts["langs"]["English"]["meanings"]["noun"].append(concept)

        for synonym in synlist:
            if (synonym != title):
                syn_concepts: Dict = deepcopy(TDV_WIKT_SCHEMA)
                syn_concepts["title"] = synonym
                concept: Dict = deepcopy(TDV_MEANING_SCHEMA)
                concept["meaning"] = title
                concept["links"] = [title]
                syn_concepts["langs"]["English"]["meanings"]["noun"].append(concept)

                for other_synonym in [syn for syn in synlist if syn != synonym]:
                    other_syn_meaning: Dict = deepcopy(TDV_MEANING_SCHEMA)
                    other_syn_meaning["meaning"] = other_synonym
                    syn_concepts["langs"]["English"]["synonyms"]["noun"].append(other_syn_meaning)

                syn_meaning = deepcopy(TDV_MEANING_SCHEMA)
                syn_meaning["meaning"] = synonym
                chem_concepts["langs"]["English"]["synonyms"]["noun"].append(syn_meaning)
                chemsyns.append(syn_concepts)

        chemsyns.append(chem_concepts)

        return chemsyns

