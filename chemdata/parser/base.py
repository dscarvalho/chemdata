from abc import ABC, abstractmethod


class ChemDataParser(ABC):
    @abstractmethod
    def parse(self, data):
        pass
