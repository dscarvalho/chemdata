__author__ = 'danilo.silva@cdts.fiocruz.br'

import uuid
import sys
import io
from typing import List, Dict
from copy import deepcopy
from .base import ChemDataParser
from csv import DictReader

attr_map = {
    "Physicochemical Properties": {
        "Formula": "Formula",
        "MW": "Molecular weight",
        "#Heavy atoms": "Num. heavy atoms",
        "#Aromatic heavy atoms": "Num. arom. heavy atoms",
        "Fraction Csp3": "Fraction Csp3",
        "#Rotatable bonds": "Num. rotatable bonds",
        "#H-bond acceptors": "Num. H-bond acceptors",
        "#H-bond donors": "Num. H-bond donors",
        "MR": "Molar Refractivity",
        "TPSA": "TPSA"
    },
    "Lipophilicity": {
        "iLOGP": "Log Po/w (iLOGP)",
        "XLOGP3": "Log Po/w (XLOGP3)",
        "WLOGP": "Log Po/w (WLOGP)",
        "MLOGP": "Log Po/w (MLOGP)",
        "Silicos-IT Log P": "Log Po/w (SILICOS-IT)",
        "Consensus Log P": "Consensus Log Po/w",
    },
    "Water Solubility": {
        "ESOL Log S": "Log S (ESOL)",
        "ESOL Solubility (mg/ml)": "ESOL Solubility (mg/ml)",
        "ESOL Solubility (mol/l)": "ESOL Solubility (mol/l)",
        "ESOL Class": "ESOL Class",
        "Ali Log S": "Log S (Ali)",
        "Ali Solubility (mg/ml)": "Ali Solubility (mg/ml)",
        "Ali Solubility (mol/l)": "Ali Solubility (mol/l)",
        "Ali Class": "Ali Class",
        "Silicos-IT LogSw": "Log S (SILICOS-IT)",
        "Silicos-IT Solubility (mg/ml)": "Silicos-IT Solubility(mg/ml)",
        "Silicos-IT Solubility (mol/l)": "Silicos-IT Solubility (mol/l)",
        "Silicos-IT class": "Silicos-IT Class",
    },
    "Pharmacokinetics": {
        "GI absorption": "GI absorption",
        "BBB permeant": "BBB permeant",
        "Pgp substrate": "P-gp substrate",
        "CYP1A2 inhibitor": "CYP1A2 inhibitor",
        "CYP2C19 inhibitor": "CYP2C19 inhibitor",
        "CYP2C9 inhibitor": "CYP2C9 inhibitor",
        "CYP2D6 inhibitor": "CYP2D6 inhibitor",
        "CYP3A4 inhibitor": "CYP3A4 inhibitor",
        "log Kp (cm/s)": "Log Kp (skin permeation)",
    },
    "Druglikeness" :
    {
        "Lipinski #violations": "Lipinski",
        "Ghose #violations": "Ghose",
        "Veber #violations": "Veber",
        "Egan #violations": "Egan",
        "Muegge #violations": "Muegge",
        "Bioavailability Score": "Bioavailability Score",
    },
    "Medicinal Chemistry" :
    {
        "PAINS #alerts": "PAINS",
        "Brenk #alerts": "Brenk",
        "Leadlikeness #violations": "Leadlikeness",
        "Synthetic Accessibility": "Synthetic accessibility",
    }
}

class SmilesSwissAdmeParser(ChemDataParser):
    def parse(self, data):
        if (data[0] == '<'):
            return data 
        else:
            csv_reader = DictReader(io.StringIO(data))
            dict_csv = dict(next(csv_reader))

            report = dict()
            report["smiles"] = dict_csv["Canonical SMILES"]

            for key_group, group in attr_map.items():
                report[key_group] = dict()
                for key_attr, attr in group.items():
                    report[key_group][attr] = dict_csv[key_attr]     

            return report


