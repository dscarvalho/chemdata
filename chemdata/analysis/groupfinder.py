import pandas as pd
from rdkit import Chem
from collections import Counter


class GroupFinder:
    def __init__(self, org_functions_filepath: str):
        self.org_functions = pd.read_csv(org_functions_filepath, delimiter='\t')
        self.dict_smarts = dict(zip(self.org_functions['Funcao'], self.org_functions['SMARTS']))
        self.matches = range(self.org_functions['Funcao'].size)

    def count_groups(self, smiles: str) -> Counter:
        dict_return = dict(zip(self.org_functions['Funcao'], self.matches))
        m = Chem.MolFromSmiles(smiles)
        for function, SMARTS in self.dict_smarts.items():
            f = Chem.MolFromSmarts(SMARTS)
            n = m.GetSubstructMatches(f)
            dict_return[function] = len(n)

        return Counter(dict_return)
