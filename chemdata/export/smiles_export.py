from .smiles_csv_export import SmilesCsvExport

class SmilesExport:

    def __init__(self, export_type, string_files, export_path):
        if export_type == "csv":
            SmilesCsvExport(string_files, export_path)
