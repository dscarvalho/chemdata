import io
import re
import csv
import glob
import json
import os.path
from pprint import pprint
from csv import DictReader
from collections import OrderedDict


class SmilesCsvExport:

    def __init__(self, string_files, export_path):
        self.export_csv(string_files, export_path)

    def export_csv(self, string_files, export_path):
        try:
            if os.path.exists(export_path):
                file_smiles = open(export_path, "r")
                lines_smiles = file_smiles.read()
                count_lines_smiles = len(lines_smiles)
                file_smiles.close()
            else:
                new_file = open(export_path, "w+")
                new_file.close()
                count_lines_smiles = 0

            files = glob.glob(string_files)

            with open(export_path, "a+") as f:
                
                for file in files:
                    data = [json.loads(line) for line in open(file, 'r')]
                    for count in range(0,len(data), 2):
                        next_count = count + 1
                        ln_swissadme = self.parser_swissadme(data[count])

                        """ ADMETSAR
                        if len(data) > next_count:
                            ln_admetsar = self.parser_admetsar(data[next_count])
                            ln_swissadme.update(ln_admetsar)
                        """

                        header = True if count_lines_smiles < 1 else False

                        self.write_line_csv(f, ln_swissadme, header)
                        count_lines_smiles += 1
            f.close()

        except Exception as e:
            pprint(e)
        

    def write_line_csv(self, file, line, header):
        countKeys = 0
        value = list()

        for key in line.keys():
            countKeys += 1
            text = "%s\t" if countKeys < len(line) else "%s\n"
            value.append(text % (key) if header else text % (line[key]))
        
        report = ''.join(value)    
        file.writelines(report)
        
            
    def parser_swissadme(self, report):
        new_report = OrderedDict()
        new_report['smiles'] = report['smiles']   
        pfx_server = 'SwissAdme_'

        for key_group, group in report.items():
            pfx_group = key_group

            if key_group == "smiles":
                continue

            for key_attr, attr in group.items():
                pfx_attr = pfx_server + pfx_group + "_" + key_attr
                prefix = re.sub("/[^a-zA-Z]/", "", pfx_attr.replace(" ", "_"))
                new_report[prefix] = attr

        return new_report  


    def parser_admetsar(self, report):
        new_report = OrderedDict()
        pfx_server = 'AdmetSar_'

        for key_table, table in report.items():
            if key_table == "smiles":
                continue

            for group in table:

                pfx_attr = pfx_server + group["Model"]
                prefix = re.sub("/[^a-zA-Z]/", "", pfx_attr.replace(" ", "_"))

                if key_table == 'classification':
                    new_report[prefix] = group['Result'] + \
                        " ("+group['Probability']+")"

                elif key_table == 'regression':
                    new_report[prefix] = group['Value'] + \
                        " "+group['Unit']

        return new_report


#if __name__ == "__main__":
#    self.export_csv()
    
