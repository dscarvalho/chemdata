__author__ = 'danilo.silva@cdts.fiocruz.br'

import time
import multiprocessing as mp
import random
import numpy as np
from typing import Tuple, Set
from joblib import Parallel, delayed, parallel_backend
from os import getpid
from csv import DictReader

from chemdata.fetcher import SmilesSwissAdmeFetcher
from chemdata.parser import SmilesSwissAdmeParser

from chemdata.fetcher import SmilesAdmetSarFetcher
from chemdata.parser import SmilesAdmetSarParser

from chemdata.data_access.json_line import JsonLinesFileStore


def get_data(smiles: Set[str], filepath: str, completed_smiles: Set[str]):
    data_store = JsonLinesFileStore(filepath + "_%d.jsonl" % getpid(), "a")
    
    #parser_admetsar = SmilesAdmetSarParser()
    #fetcher_admetsar = SmilesAdmetSarFetcher(parser_admetsar)

    parser_swissadme = SmilesSwissAdmeParser()
    fetcher_swissadme = SmilesSwissAdmeFetcher(parser_swissadme)

    for smile in smiles.difference(completed_smiles):
        try:
            time.sleep(random.random() * 2)
            synset_data_swissadme = fetcher_swissadme.fetch(smile)
            #synset_data_admetsar = fetcher_admetsar.fetch(smile)

            if (synset_data_swissadme is not None):
                synset_data_swissadme['smiles'] = smile
                data_store.writeline(synset_data_swissadme)

            #if (synset_data_admetsar is not None):
            #    synset_data_admetsar['smiles'] = smile
            #    data_store.writeline(synset_data_admetsar)

            #for doc in synset_data:
            #    data_store.writeline(doc)

            print("#%s;" % smile)
        except Exception as e:
            print(e, e.args)

    data_store.close()


def set_jobs(store_filepath: str, smiles_csv_filepath: str, completed_smiles: Set[str]):
    num_processes = 30 #mp.cpu_count()
    mp.set_start_method("spawn")
    smiles_list = []

    with open(smiles_csv_filepath) as smiles_csv_file:
        csv_reader = DictReader(smiles_csv_file, delimiter=',')
        for row in csv_reader:
            smiles_list.append(row["Smiles"])

    #print([s for s in smiles_list if (s.startswith("O="))])
    #exit(0)
    #cid_sets = np.array_split(range(1, PUGVIEW_CONST["CUR_MAX_CID"]), 1000000)
    #cid_ranges = [(cid_set[0], cid_set[-1]) for cid_set in cid_sets]

    smiles_sets = np.array_split(smiles_list, 1000)

    with parallel_backend("multiprocessing", n_jobs=num_processes):
        Parallel()(delayed(get_data)(set(smiles_set), store_filepath, completed_smiles)
                   for smiles_set in smiles_sets)
