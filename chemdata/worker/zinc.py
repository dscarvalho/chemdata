__author__ = 'danilo.silva@cdts.fiocruz.br'

import sys
import time
import multiprocessing as mp
import random
import gzip
import numpy as np
from typing import Tuple, Set, List
from joblib import Parallel, delayed, parallel_backend
from os import getpid
from csv import DictReader

from chemdata.fetcher import ZincFetcher
from chemdata.data_access.chemdata_pg import ChemDataStore, Zinc, IntegrityError


def get_data(filename: str, connstring: str):
    data_store = ChemDataStore(connstring)
    transaction_size = 0

    with gzip.open(filename, mode="rt") as csv_file:
        csv_reader = DictReader(csv_file, delimiter="\t")

        for row in csv_reader:
            try:
                zinc_obj = Zinc()
                zinc_obj.zinc_id = int(row["zinc_id"][4:])
                zinc_obj.smiles = row["smiles"]
                zinc_obj.zinc_features = row["features"] if row["features"].strip() else None
                zinc_obj.molweight = float(row["mwt"])
                zinc_obj.logp = float(row["logp"])

                data_store.insert_zinc(zinc_obj)
                print("Added", row["zinc_id"])

                transaction_size += 1
                if (transaction_size > 100000):
                    data_store.commit()
                    transaction_size = 0
                #except IntegrityError:
                #    print("Existing", (row["zinc_id"], row["smiles"]))
                #    data_store.rollback()
            except:
                print(sys.exc_info(), file=sys.stderr)

    data_store.commit()
    data_store.close()


def set_jobs(filenames: List[str], connstring: str):
    num_processes = mp.cpu_count()
    # mp.set_start_method("spawn")

    with parallel_backend("multiprocessing", n_jobs=num_processes):
        Parallel()(delayed(get_data)(filename, connstring) for filename in filenames)

