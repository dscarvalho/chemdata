__author__ = 'danilo.silva@cdts.fiocruz.br'

import time
import multiprocessing as mp
import random
import numpy as np
from typing import Tuple, Set
from joblib import Parallel, delayed, parallel_backend
from os import getpid

from chemdata.fetcher import PUGViewFetcher
from chemdata.parser import PUGViewSynsetParser
from chemdata.data_access.json_line import JsonLinesFileStore


PUGVIEW_CONST = {
    "CUR_MAX_CID": 138753057
}


def get_data(cid_range: Tuple[int], filepath: str, completed_cids: Set[int]):
    data_store = JsonLinesFileStore(filepath + "_%d.jsonl" % getpid(), "a")
    parser = PUGViewSynsetParser()
    fetcher = PUGViewFetcher(parser)

    for cid in set(range(*cid_range)).difference(completed_cids):
        try:
            time.sleep(random.random() * 2) 
            synset_data = fetcher.fetch(cid)

            if (synset_data is not None):
                data_store.writeline(synset_data)

            # for doc in synset_data:
            #     data_store.writeline(doc)

            print("#%d;" % cid)
        except Exception as e:
            print(e, e.args)

    data_store.close()


def set_jobs(store_filepath: str, completed_cids: Set[int]):
    num_processes = 16 #mp.cpu_count()
    # mp.set_start_method("spawn")
    cid_sets = np.array_split(range(1, PUGVIEW_CONST["CUR_MAX_CID"]), 1000000)
    cid_ranges = [(cid_set[0], cid_set[-1]) for cid_set in cid_sets]

    with parallel_backend("multiprocessing", n_jobs=num_processes):
        Parallel()(delayed(get_data)(cid_range, store_filepath, completed_cids) for cid_range in cid_ranges)

