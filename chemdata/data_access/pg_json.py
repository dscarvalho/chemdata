__author__ = 'danilo.silva@cdts.fiocruz.br'

import json
import psycopg2

POSTGRE_INSERT = """insert into %s (document) """
POSTGRE_JSON_INS = """values %s"""


class PostgreJsonStore:
    def __init__(self, connstring: str):
        self.conn = psycopg2.connect(connstring)
        self.cursor = self.conn.cursor()

    def insert(self, table: str, doc: dict):
        self.cursor.execute((POSTGRE_INSERT % table) + POSTGRE_JSON_INS, (json.dumps(doc),))
