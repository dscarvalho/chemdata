__author__ = 'danilo.silva@cdts.fiocruz.br'

import os
import gzip

ATTR_SDF_LABELS = {
    "PUBCHEM_CACTVS_HBOND_ACCEPTOR": "num_h_bond_acceptors",
    "PUBCHEM_CACTVS_HBOND_DONOR": "num_h_bond_donors",
    "PUBCHEM_CACTVS_ROTATABLE_BOND": "num_rotatable_bonds",
    "PUBCHEM_XLOGP3": "xlogp3",
    "PUBCHEM_XLOGP3_AA": "xlogp3",
    "PUBCHEM_MOLECULAR_WEIGHT": "molecular_weight",
    "PUBCHEM_CACTVS_TPSA": "tpsa",
    "PUBCHEM_HEAVY_ATOM_COUNT": "num_heavy_atoms",
    "PUBCHEM_MOLECULAR_FORMULA": "formula"
}


class PubChemMappingReader:
    def __init__(self, filepath):
        self.filestore = open(filepath)

    def __iter__(self):
        return self

    def __next__(self):
        line = self.filestore.readline().strip()

        if (line == ""):
            raise StopIteration

        cols = line.split("\t")
        cid = int(cols[0])
        attrs = cols[1:]

        return (cid, attrs)


class PubChemPropertiesReader:
    def __init__(self, sdf_dir: str):
        self.sdf_dir = sdf_dir
        self.sdf_files = sorted([filename for filename in os.listdir(sdf_dir) if (filename.endswith(".sdf.gz"))])

        self.sdf_files = self.sdf_files[self.sdf_files.index("Compound_078325001_078350000.sdf.gz") + 1:]

        self.curfile_idx = -1
        self.curfile = None
        self.next_file()
        self.cur_compound = {"cid": 0}

    def __iter__(self):
        return self

    def __next__(self):
        line = self.next_line()

        while (line != "> <PUBCHEM_COMPOUND_CID>"):
            line = self.next_line()

        line = self.next_line()
        self.cur_compound["cid"] = int(line)

        while (line != "$$$$"):
            line = self.next_line()

            for attr_label in ATTR_SDF_LABELS:
                if (line == "> <%s>" % attr_label):
                    line = self.next_line()
                    self.cur_compound[ATTR_SDF_LABELS[attr_label]] = line

        return self.cur_compound

    def next_line(self) -> str:
        line = self.curfile.readline().decode("ascii")

        if (line == ""):
            hasnext = self.next_file()

            if (not hasnext):
                raise StopIteration
            else:
                line = self.curfile.readline().decode("ascii")

        return line.strip()

    def next_file(self) -> bool:
        self.curfile_idx += 1
        if (self.curfile_idx >= len(self.sdf_files)):
            return False
        else:
            if (self.curfile is not None):
                self.curfile.close()

            self.curfile = gzip.open(os.path.join(self.sdf_dir, self.sdf_files[self.curfile_idx]), "r")
            print("Opening file:", self.sdf_files[self.curfile_idx])
            return True

