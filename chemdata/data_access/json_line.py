__author__ = 'danilo.silva@cdts.fiocruz.br'

import json
from typing import Dict


class JsonLinesFileStore:
    def __init__(self, filepath: str, mode: str):
        self.filestore = open(filepath, mode)

    def readline(self) -> Dict:
        return json.loads(self.filestore.readline())

    def writeline(self, doc: Dict):
        self.filestore.write(json.dumps(doc) + "\n")
        self.filestore.flush()

    def close(self):
        self.filestore.close()
