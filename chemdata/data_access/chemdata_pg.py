__author__ = 'danilo.silva@cdts.fiocruz.br'

from typing import List, Dict

from sqlalchemy import Column, Index, ForeignKey, Integer, BigInteger, String, Float, Boolean, Text, func
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.exc import IntegrityError

Base = declarative_base()
metadata = Base.metadata


class Compound(Base):
    __tablename__ = 'compounds'
    __table_args__ = (
        # Index('idx_compound_cid', 'cid'),
        # Index('idx_compound_title', 'title'),
        # Index('idx_compound_smiles', 'smiles'),
    )

    id = Column(Integer, primary_key=True)  # ID
    cid = Column(Integer, nullable=False)  # PubChem CID
    title = Column(Text, nullable=False)  # Compound name
    smiles = Column(Text, nullable=True)  # SMILES identifier
    iupac = Column(Text, nullable=True)  # IUPAC Name
    inchi = Column(Text, nullable=True)  # InChI
    inchi_key = Column(Text, nullable=True)  # InChI Key
    cas = Column(Text, nullable=True)  # CAS identifier
    formula = Column(Text, nullable=True)  # Formula


class CompoundAttributesReference(Base):
    __tablename__ = 'compound_attr_references'
    __table_args__ = (
        # Index('idx_compound_attr_id', 'id'),
        # Index('idx_compound_attr_name', 'name'),
    )
    id = Column(Integer, primary_key=True)  # ID
    name = Column(Text, nullable=True)  # Reference name


class CompoundAttributes(Base):
    __tablename__ = 'compound_attributes'
    __table_args__ = (
        # Index('idx_compound_attr_compound_id', 'compound_id'),
        # Index('idx_compound_attr_ref', 'reference'),
    )
    compound_id = Column(Integer, ForeignKey(Compound.id), primary_key=True, nullable=False)
    reference = Column(Integer, ForeignKey(CompoundAttributesReference.id), primary_key=True, nullable=True)  # Identifier for the attributes reference
    molecular_weight = Column(Float, nullable=True)  # Molecular weight
    molecular_weight_unit = Column(String(10), nullable=True)  # Molecular weight unit
    num_heavy_atoms = Column(Integer, nullable=True)  # Num. heavy atoms
    num_arom_heavy_atoms = Column(Integer, nullable=True)  # Num. arom. heavy atoms
    fraction_csp3 = Column(Float, nullable=True)  # Fraction Csp3
    num_rotatable_bonds = Column(Integer, nullable=True)  # Num. rotatable bonds
    num_h_bond_acceptors = Column(Integer, nullable=True)  # Num. H-bond acceptors
    num_h_bond_donors = Column(Integer, nullable=True)  # Num. H-bond donors
    molar_refractivity = Column(Float, nullable=True)  # Molar Refractivity
    tpsa = Column(Float, nullable=True)  # TPSA
    tpsa_unit = Column(String(10), nullable=True)

    ilogp = Column(Float, nullable=True)  # Log Po/w (iLOGP)
    xlogp3 = Column(Float, nullable=True)  # Log Po/w (XLOGP3)
    wlogp = Column(Float, nullable=True)  # Log Po/w (WLOGP)
    mlogp = Column(Float, nullable=True)  # Log Po/w (MLOGP)
    silicos_it = Column(Float, nullable=True)  # Log Po/w (SILICOS-IT)
    consensus_log_p_ow = Column(Float, nullable=True)  # Consensus Log Po/w

    log_s_esol = Column(Float, nullable=True)  # Log S (ESOL)
    solubility_esol_mg_ml = Column(Float, nullable=True)  # Solubility Mg/Ml
    solubility_esol_mol_l = Column(Float, nullable=True)  # Solubility Mol/l
    class_esol = Column(Text, nullable=True)  # Class
    log_s_ali = Column(Float, nullable=True)  # Log S (Ali)
    solubility_ali_mg_ml = Column(Float, nullable=True)  # Solubility Mg/Ml
    solubility_ali_mol_l = Column(Float, nullable=True)  # Solubility Mol/l
    class_ali = Column(Text, nullable=True)  # Class
    log_s_silicos_it = Column(Float, nullable=True)  # Log S (SILICOS-IT)
    solubility_silicos_it_mg_ml = Column(Float, nullable=True)  # Solubility Mg/Ml
    solubility_silicos_it_mol_l = Column(Float, nullable=True)  # Solubility Mol/l
    class_silicos_it = Column(Text, nullable=True)  # Class
    
    gi_absorption = Column(Text, nullable=True)  # GI absorption
    bbb_permeant = Column(Boolean, nullable=True)  # BBB permeant
    p_gp_substrate = Column(Boolean, nullable=True)  # P-gp substrate
    p_gp_inhibitor = Column(Boolean, nullable=True)  # P-gp inhibitor
    cyp1a2_inhibitor = Column(Boolean, nullable=True)  # CYP1A2 inhibitor
    cyp2c19_inhibitor = Column(Boolean, nullable=True)  # CYP2C19 inhibitor
    cyp2c9_inhibitor = Column(Boolean, nullable=True)  # CYP2C9 inhibitor
    cyp2d6_inhibitor = Column(Boolean, nullable=True)  # CYP2D6 inhibitor
    cyp3a4_inhibitor = Column(Boolean, nullable=True)  # CYP3A4 inhibitor
    cyp2c9_substrate = Column(Boolean, nullable=True)  # CYP2C9 substrate
    cyp2d6_substrate = Column(Boolean, nullable=True)  # CYP2D6 substrate
    cyp3a4_substrate = Column(Boolean, nullable=True)  # CYP3A4 substrate
    skin_permeation = Column(Float, nullable=True)  # Log Kp (skin permeation)

    lipinski = Column(Integer, nullable=True)  # Lipinski
    ghose = Column(Boolean, nullable=True)  # Ghose
    veber = Column(Boolean, nullable=True)  # Veber
    egan = Column(Boolean, nullable=True)  # Egan
    muegge = Column(Boolean, nullable=True)  # Muegge
    bioavailability_score = Column(Float, nullable=True)  # Bioavailability Score

    pains = Column(Integer, nullable=True)  # PAINS
    brenk = Column(Integer, nullable=True)  # Brenk
    leadlikeness = Column(Integer, nullable=True)  # Leadlikeness
    synthetic_accessibility = Column(Float, nullable=True)  # Synthetic accessibility


    caco_2_permeability = Column(Text, nullable=True)  # Caco-2 Permeability
    renal_organic_cation_transporter = Column(Boolean, nullable=True)  # Renal Organic Cation Transporter
    subcellular_localization = Column(Text, nullable=True)  # Subcellular localization
    cyp_inhibitory_promiscuity = Column(Text, nullable=True)  # CYP Inhibitory Promiscuity
    human_ether_a_go_go_related_gene_inhibition = Column(Text, nullable=True)  # Human Ether-a-go-go-Related Gene Inhibition
    ames_toxicity = Column(Text, nullable=True)  # AMES Toxicity
    carcinogens = Column(Text, nullable=True)  # Carcinogens
    fish_toxicity = Column(Text, nullable=True)  # Fish Toxicity
    tetrahymena_pyriformis_toxicity = Column(Text, nullable=True)  # Tetrahymena Pyriformis Toxicity
    honey_bee_toxicity = Column(Text, nullable=True)  # Honey Bee Toxicity
    biodegradation = Column(Text, nullable=True)  # Biodegradation
    acute_oral_toxicity = Column(Text, nullable=True)  # Acute Oral Toxicity
    carcinogenicity_three_class = Column(Text, nullable=True)  # Carcinogenicity (Three-class)
    regression_aqueous_solubility = Column(Float, nullable=True)  # Aqueous solubility -- Regression
    regression_caco_2_permeability = Column(Float, nullable=True)  # Synthetic accessibility
    regression_rat_acute_toxicity = Column(Float, nullable=True)  # Synthetic accessibility
    regression_fish_toxicity = Column(Float, nullable=True)  # Synthetic accessibility
    regression_tetrahymena_pyriformis_toxicity = Column(Float, nullable=True)  # Synthetic accessibility

    attr_groups = {
        "Physicochemical_Properties": [
            "molecular_weight",
            "molecular_weight_unit",
            "num_heavy_atoms",
            "num_arom_heavy_atoms",
            "fraction_csp3",
            "num_rotatable_bonds",
            "num_h_bond_acceptors",
            "num_h_bond_donors",
            "molar_refractivity",
            "tpsa",
            "tpsa_unit",
            "ilogp",
            "xlogp3",
            "wlogp",
            "mlogp",
            "silicos_it",
            "consensus_log_p_ow",
            "log_s_esol",
            "solubility_esol_mg_ml",
            "solubility_esol_mol_l",
            "class_esol",
            "log_s_ali",
            "solubility_ali_mg_ml",
            "solubility_ali_mol_l",
            "class_ali",
            "log_s_silicos_it",
            "solubility_silicos_it_mg_ml",
            "solubility_silicos_it_mol_l",
            "class_silicos_it"

        ],
        "Pharmacokinetics": [
            "gi_absorption",
            "bbb_permeant",
            "p_gp_substrate",
            "p_gp_inhibitor",
            "cyp1a2_inhibitor",
            "cyp2c19_inhibitor",
            "cyp2c9_inhibitor",
            "cyp2d6_inhibitor",
            "cyp3a4_inhibitor",
            "cyp2c9_substrate",
            "cyp2d6_substrate",
            "cyp3a4_substrate",
            "skin_permeation",
            "caco_2_permeability",
            "renal_organic_cation_transporter",
            "subcellular_localization",
            "cyp_inhibitory_promiscuity",
            "human_ether_a_go_go_related_gene_inhibition",
            "ames_toxicity",
            "fish_toxicity",
            "tetrahymena_pyriformis_toxicity",
            "honey_bee_toxicity",
            "biodegradation",
            "regression_aqueous_solubility",
            "regression_caco_2_permeability",
            "regression_rat_acute_toxicity",
            "regression_fish_toxicity",
            "regression_tetrahymena_pyriformis_toxicity"
        ],
        "Druglikeness": [
            "lipinski",
            "ghose",
            "veber",
            "egan",
            "muegge",
            "bioavailability_score"
        ],
        "Medicinal_Chemistry": [
            "carcinogens",
            "carcinogenicity_three_class",
            "acute_oral_toxicity",
            "pains",
            "brenk",
            "leadlikeness",
            "synthetic_accessibility"
        ]
    }


class CompoundSynonym(Base):
    __tablename__ = 'compound_synonyms'
    __table_args__ = (
        # Index('idx_compsyn_compound_id', 'compound_id'),
        # Index('idx_compsyn_synonym', 'synonym'),
    )

    compound_id = Column(Integer, ForeignKey(Compound.id), primary_key=True, nullable=False)
    synonym = Column(Text, primary_key=True, nullable=False)  # Compound synonym


class CompoundDescription(Base):
    __tablename__ = 'compound_descriptions'
    __table_args__ = (
        # Index('idx_compdescr_compound_id', 'compound_id'),
        # Index('idx_compsyn_synonym', 'synonym'),
    )

    id = Column(Integer, primary_key=True)  # ID
    compound_id = Column(Integer, ForeignKey(Compound.id), nullable=False)
    description = Column(Text, nullable=False)  # Compound description


class CompoundPatent(Base):
    __tablename__ = 'compound_patents'
    __table_args__ = (
        # Index('idx_comppat_compound_id', 'compound_id'),
        # Index('idx_comppat_patent', 'patent'),
    )

    compound_id = Column(Integer, ForeignKey(Compound.id), primary_key=True, nullable=False)
    patent = Column(Text, primary_key=True, nullable=False)  # Compound description


class Zinc(Base):
    __tablename__ = 'zinc'
    __table_args__ = (
        # Index('idx_zinc_code', 'zinc_code'),
        # Index('idx_zinc_smiles', 'smiles'),
    )

    zinc_id = Column(BigInteger, primary_key=True)  # ZINC ID
    smiles = Column(String, nullable=False)  # SMILES identifier
    zinc_features = Column(String, nullable=True)  # ZINC features
    molweight = Column(Float, nullable=False)
    logp = Column(Float, nullable=False)


class ChemDataStore:
    def __init__(self, connstring: str):
        self.engine = engine = create_engine(connstring)
        self.session = sessionmaker(bind=engine)()
        self.gen_database()
        self.cur_max_id = 0 if (self.get_max_id() is None) else self.get_max_id()

    def gen_database(self):
        metadata.create_all(self.engine)

    def commit(self):
        self.session.commit()

    def close(self):
        self.session.close()
        self.engine.dispose()

    def rollback(self):
        self.session.rollback()

    def get_max_id(self) -> int:
        return self.session.query(func.max(Compound.id)).scalar()

    def insert_compound(self, compound: Compound):
        self.session.add(compound)

    def insert_compound_synonym(self, compound_synonym: CompoundSynonym):
        self.session.add(compound_synonym)

    def insert_compound_patent(self, compound_patent: CompoundPatent):
        self.session.add(compound_patent)

    def insert_compound_attributes(self, compound_attributes: CompoundAttributes):
        self.session.add(compound_attributes)

    def insert_compound_attributes_reference(self, compound_attributes_reference: CompoundAttributesReference):
        self.session.add(compound_attributes_reference)

    def insert_zinc(self, zinc: Zinc):
        self.session.add(zinc)

    def update_compound_mappings(self, compound_mappings: List[Dict]):
        self.session.bulk_update_mappings(Compound, compound_mappings)
        self.session.commit()

    def get_compound_by_cid(self, cid: int) -> Compound:
        compound = self.session.query(Compound).filter(Compound.cid == cid).first()

        return compound

    def get_compound_ids_by_cid(self, cids: List[int]) -> List[Compound]:
        compounds = self.session.query(Compound.id, Compound.cid).filter(Compound.cid.in_(cids))

        return compounds

    def get_compound_by_smiles(self, smiles: str) -> Compound:
        compound = self.session.query(Compound).filter(Compound.smiles == smiles).first()

        return compound

    def get_compound_by_name(self, name: str) -> Compound:
        try:
            synset_id = self.session.query(CompoundSynonym).filter(func.lower(CompoundSynonym.synonym).like(name.lower())).first().compound_id
            compound = self.session.query(Compound).filter(Compound.id == synset_id).first()

            return compound
        except AttributeError:
            return None

    def get_compounds_by_name(self, name: str) -> List[Compound]:
        synonyms = self.session.query(CompoundSynonym).filter(func.lower(CompoundSynonym.synonym).like(name.lower() + "%")).all()
        synset_ids = set([synonym.compound_id for synonym in synonyms])

        compounds = self.session.query(Compound).filter(Compound.id.in_(synset_ids)).all()

        return compounds

    def get_synonyms_by_id(self, compound_id: int) -> List[str]:
        synset = self.session.query(CompoundSynonym).filter(CompoundSynonym.compound_id == compound_id).all()

        return [syn.synonym for syn in synset]

    def get_synonyms_by_name(self, name: str) -> List[str]:
        synset_id = self.session.query(CompoundSynonym).filter(func.lower(CompoundSynonym.synonym).like(name.lower())).first().compound_id
        synset = self.session.query(CompoundSynonym).filter(CompoundSynonym.compound_id == synset_id).all()

        return [syn.synonym for syn in synset]

    def get_synonyms_by_smiles(self, smiles: str) -> List[str]:
        compound = self.session.query(Compound).filter(Compound.smiles == smiles).first()
        synset = self.session.query(CompoundSynonym).filter(CompoundSynonym.compound_id == compound.id).all()

        return [syn.synonym for syn in synset]

    def get_patents_by_id(self, compound_id: int) -> List[str]:
        patent_set = self.session.query(CompoundPatent).filter(CompoundPatent.compound_id == compound_id).all()

        return [pat.patent for pat in patent_set]

    def get_patents_by_name(self, name: str) -> List[str]:
        synset_id = self.session.query(CompoundSynonym).filter(CompoundSynonym.synonym == name).first().compound_id
        patent_set = self.session.query(CompoundPatent).filter(CompoundPatent.compound_id == synset_id).all()

        return [pat.patent for pat in patent_set]

    def get_patents_by_smiles(self, smiles: str) -> List[str]:
        compound = self.session.query(Compound).filter(Compound.smiles == smiles).first()
        patent_set = self.session.query(CompoundPatent).filter(CompoundPatent.compound_id == compound.id).all()

        return [pat.patent for pat in patent_set]

    def get_compound_attributes_reference_by_name(self, name: String) -> CompoundAttributesReference:
        compound_attributes_reference = self.session.query(CompoundAttributesReference).filter(
            CompoundAttributesReference.name == name).first()

        return compound_attributes_reference
        
    def get_compound_attributes(self, compound_id: int) -> List[CompoundAttributes]:
        attributes = self.session.query(CompoundAttributes).filter(CompoundAttributes.compound_id == compound_id).all()

        return list(attributes)

